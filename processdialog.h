#ifndef PROCESSDIALOG_H
#define PROCESSDIALOG_H

#include <QDialog>

namespace Ui {
class processdialog;
}

class processdialog : public QDialog
{
    Q_OBJECT

public:
    explicit processdialog(QWidget *parent = nullptr);
    ~processdialog();

private slots:
    void on_cancel_clicked();

private:
    Ui::processdialog *ui;
};

#endif // PROCESSDIALOG_H
