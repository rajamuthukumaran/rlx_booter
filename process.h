#ifndef PROCESS_H
#define PROCESS_H

#include <QProcess>
#include <QString>
#include <QMessageBox>
#include <QProgressBar>
#include <QFile>
#include <QDir>

class Process : public QObject
{
public:
    bool RootChecker();
    QString checksum(QString,QString,int);
    bool UnMount(QString);
    bool DevFormat(QString,bool);
    bool LinuxBooter(QString,QString);
    bool WindowsBooter(QString,QString);
    void ProgressBox(bool);
    void ProcessKill();

public slots:
    void readyReadStandardOutput();

private:
    QProcess shll;
    void CopyFiles(QString,QString);
};

#endif // PROCESS_H
