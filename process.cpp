#include "process.h"

bool Process::RootChecker(){
    QProcess rchk;
    QString who("whoami");
    rchk.start(who);
    rchk.waitForFinished(30);
    QString result = rchk.readAllStandardOutput().left(4);
    if(result == "root"){
        return true;
    }else{
        return false;
    }
}

QString Process::checksum(QString fileloc, QString methd, int sum)
{
    QString result;
    QProcess checksumer;
    QStringList arguments;
    arguments << fileloc;

    checksumer.start(methd, arguments);
    checksumer.waitForFinished(-1);
    result = checksumer.readAllStandardOutput().left(sum);

    return result;
}

bool checker(QString result,QString check){
    if(result == check){
        return true;
    }else{
        return false;
    }
}

bool Process::UnMount(QString device){
    QString cmd("umount");
    QStringList arg;
    arg << "umount" << device;
    shll.start(cmd,arg);
    shll.waitForFinished();
    QString busy_err = shll.readAllStandardError().right(6).remove(4,2);
    if(busy_err == "busy"){
        return false;
    }else{
        return true;
    }
}

bool Process::DevFormat(QString device,bool ntfs){
    QString cmd("mkfs.");
    QStringList arg;
    if(ntfs){

        cmd += "ntfs";

//        if( device.length() == 8 ){
////            QMessageBox::question(this,"Warning","No partition found in the device. Would you like to create a partition? (if there is alreay a partition please select it instead of device)",standardButtons(),setDefaultButton(QMessageBox::No));
//            QString part("parted");
//            QStringList part_arg;
//            part_arg << "-a" << "optimal" << device << "mkpart" << "ntfs" << "0%" << "100%";
//            shll.start(part,part_arg);
//            shll.waitForFinished(-1);
//            QString part_err = shll.readAllStandardError().left(5);
//            if( part_err == "Error" ) {return false;}
//        }

        arg << "-Q" << device << "-L" << "Rlx Booter";

    }else{

        cmd += "vfat";

//        if( device.length() > 8 ){
//            device = device.remove(8,device.length() - 8);
//        }

        arg << "-I" << device << "-n" << "Rlx Booter";
    }
    shll.start(cmd,arg);
    shll.waitForFinished(-1);
    return true;
}


bool Process::LinuxBooter(QString device, QString fileloc){
    QString cmd("dd");
    QStringList arg;
    arg << "if=" << fileloc << "of=" << device << "bs=16M";
    qInfo("DD command started");
    shll.start(cmd,arg);
//    connect(&shll,SIGNAL(readyReadStandardOutput()),this,SLOT(readyReadStandardOutput()));
    shll.waitForFinished(-1);
    qInfo("DD command finished");
    return true;
}

bool Process::WindowsBooter(QString device, QString fileloc){
    QString cmd1("mkdir"), cmd2("mount"), folder("/tmp/rlx_temp"), iso("/tempiso"), usb("/tempusb");
    QString src(folder+iso), dest(folder+usb);
    QStringList arg, arg_iso, arg_usb;
    arg << folder << src << dest;
    arg_usb << device << dest;
    arg_iso << fileloc << src << "-o" << "loop";
    shll.start(cmd1,arg);
    shll.waitForFinished(-1);
    qInfo("Created tmp folders");
    shll.start(cmd2,arg_usb);
    shll.waitForFinished(-1);
    qInfo("mounted usb");
    shll.start(cmd2,arg_iso);
    shll.waitForFinished(-1);
    qInfo("mounted iso");

    CopyFiles(src,dest);
    qInfo("copied files");

    return true;
}

void Process::CopyFiles(QString src, QString dest){
    QDir dir(src);
    if(!dir.exists()){
        return;
    }

    foreach (QString d, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        QString dst_path = dest + QDir::separator() + d;
        QString src_path = src + QDir::separator() + d;
        dir.mkpath(dst_path);
        CopyFiles(src_path,dst_path);
    }

    foreach (QString f, dir.entryList(QDir::Files)) {
        QString dst_file = dest + QDir::separator() + f;
        QString src_file = src + QDir::separator() + f;
        QFile::copy(src_file,dst_file);
    }
}

void Process::ProcessKill(){
    shll.kill();
}
