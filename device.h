#ifndef DEVICE_H
#define DEVICE_H

#include <QMessageBox>
#include <libusb-1.0/libusb.h>

class device
{
public:
    device();

    void DeviceLoader();
    void DeviceAdder(libusb_device*);
};

#endif // DEVICE_H
