#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "process.h"
#include "processdialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    deviceLoader();
    rootchk();
}

MainWindow::~MainWindow()
{
    delete ui;
}

//-------------------------- Start up task ----------------------------------------------------

void MainWindow::deviceLoader()
{
    ui->sel_device_btn->addItems(QDir("/dev").entryList(QDir::System).filter("sd"));
}

void MainWindow::rootchk(){
    Process prcss;
    if(!prcss.RootChecker()){
        ui->start_btn->setEnabled(false);
        ui->Windows->setEnabled(false);
        ui->Linux->setEnabled(false);
        ui->sel_device_btn->setEnabled(false);
        ui->format->setEnabled(false);
        QMessageBox::warning(this,"Error","Need root to open as root to make bootable image");
        ui->statusBar->showMessage("User");
    }
}

//------------------------------- functions ------------------------------------------------------

void MainWindow::getFile()
{
    QString img_path = QFileDialog::getOpenFileName(this,"Select the OS image");
    ui->img_brwsr->setText(img_path);
}

bool MainWindow::confirmation(QString msg, QString title){
    QMessageBox msgconfm;
    msgconfm.setWindowTitle(title);
    msgconfm.setText(msg);
    msgconfm.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgconfm.setDefaultButton(QMessageBox::No);
    int confm = msgconfm.exec();
    if(confm == QMessageBox::Yes){
        return true;
    }
    else {
        return false;
    }
}

bool MainWindow::chksum(){
    Process prcss;
    QString fileloc(ui->img_brwsr->text());
    QString check = ui->checkvalue->text();
    QString methd;
    int sum;

    if(ui->rmd5->isChecked()) {
        methd = "md5sum";
        sum = 32;
    }
    else if(ui->rsha1->isChecked()) {
        methd = "sha1sum";
        sum = 40;
    }
    else if(ui->rsha256->isChecked()) {
        methd = "sha256sum";
        sum = 64;
    }
    else {
        methd = "";
        sum = 0;
    }

    QString result(prcss.checksum(fileloc,methd,sum));

    ui->checksum->setText(result);

    if( check == result ){
        ui->checksum->setStyleSheet("QLabel {color: green;}");
        return true;
    }
    else if ( check.isEmpty() ) {
        ui->checksum->setStyleSheet("QLabel {color: #fff;}");
    }
    else if ( check != result ) {
        ui->checksum->setStyleSheet("QLabel {color: red;}");
        return false;
    }

    return true;
}

void MainWindow::disableAll(){
    ui->start_btn->setEnabled(false);
    ui->Windows->setEnabled(false);
    ui->Linux->setEnabled(false);
    ui->sel_device_btn->setEnabled(false);
    ui->format->setEnabled(false);
    ui->brws_btn->setEnabled(false);
    ui->img_brwsr->setEnabled(false);
    ui->check_btn->setEnabled(false);
    ui->checkvalue->setEnabled(false);
    ui->rno->setEnabled(false);
    ui->rmd5->setEnabled(false);
    ui->rsha1->setEnabled(false);
    ui->rsha256->setEnabled(false);
}

void MainWindow::enableAll(){
    ui->start_btn->setEnabled(true);
    ui->Windows->setEnabled(true);
    ui->Linux->setEnabled(true);
    ui->sel_device_btn->setEnabled(true);
    ui->format->setEnabled(true);
    ui->brws_btn->setEnabled(true);
    ui->img_brwsr->setEnabled(true);
    ui->check_btn->setEnabled(true);
    ui->checkvalue->setEnabled(true);
    ui->rno->setEnabled(true);
    ui->rmd5->setEnabled(true);
    ui->rsha1->setEnabled(true);
    ui->rsha256->setEnabled(true);
}

//---------------------------- Process --------------------------------------------------------

void MainWindow::liveBooter()
{
    Process prcss;
    processdialog pd;

    QString fileloc(ui->img_brwsr->text()), device("/dev/"+ui->sel_device_btn->currentText());
    QString msg;
    bool proceed = false;
    if(ui->format->isChecked()){
        msg = "The device "+device+" will be formatted and all the data will be erased. Do you want to continue?";
        proceed = confirmation(msg,"Confirmation");
    }else if(ui->Linux->isChecked() && !ui->format->isChecked()){
        proceed = confirmation("We need to format the device to make it linux bootable USB. Do you wish to continue?","Warning");
        if(proceed){
            ui->format->setChecked(1);
        }
    }else{
        proceed = confirmation("Please make sure the device is formated as ntfs with gpt table. If not please check Format. Do you wish to continue unformatted?.","Confirmation");
    }

    if(proceed) {
        disableAll();
//        pd.setModal(true);
//        pd.exec();

        if(prcss.UnMount(device)){
            ui->statusBar->showMessage("Device unmounted");

            if(ui->Linux->isChecked()){
//                if(prcss.DevFormat(device, false)){
                    ui->statusBar->showMessage("Formatted");

                    ui->statusBar->showMessage("Processing...");

                    if(prcss.LinuxBooter(device,fileloc)){
//                        pd.close();
                        enableAll();
                        ui->statusBar->showMessage("Finished");
                        ui->statusBar->showMessage(fileloc);
                    }else{
                        QMessageBox::critical(this,"Error","There was a error while writing the image to USB.");
//                        pd.close();
                        enableAll();
                    }

//                }else {
//                    QMessageBox::critical(this,"Error","There was a error while formatting the USB.");
////                    pd.close();
//                    enableAll();
//                }
            }

            else if(ui->Windows->isChecked()){
                QMessageBox::warning(this,"Warning","Currently the windows bootable image will only work in UEFI systems");

                if(ui->format->isChecked()){
                    if(prcss.DevFormat(device, true)){
                        ui->statusBar->showMessage("Formatted");
                    }else{
                        QMessageBox::critical(this,"Error","Unable to create partiton. Please manually create a partition to boot the image.");
//                        pd.close();
                        enableAll();
                    }
                }

                ui->statusBar->showMessage("Processing...");

                if(prcss.WindowsBooter(device,fileloc)){
//                    pd.close();
                    enableAll();
                    ui->statusBar->showMessage("Finished");
                }else{
                    QMessageBox::critical(this,"Error","There was a error while writing the image to USB.");
//                    pd.close();
                    enableAll();
                }
            }

        }else{
            QMessageBox::critical(this,"Error","Device is busy");
//            pd.close();
            enableAll();
        }
    }
}

//------------------------------- button functions -----------------------------------

void MainWindow::on_check_btn_clicked()
{
    QClipboard *clipboard = QGuiApplication::clipboard();
    QString check = clipboard->text();

    ui->checkvalue->setText(check);

    if(ui->img_brwsr->text().isEmpty()){
        ui->checksum->setText("");
        return;
    }
    else {
        chksum();
    }
}

void MainWindow::on_brws_btn_clicked()
{
    getFile();
}

void MainWindow::on_start_btn_clicked()
{
    if(chksum()) {
        ui->checksum->setText("Will flash");
        liveBooter();
    }
    else {
        ui->checksum->setText("Will not flash");
    }
}

//void MainWindow::dropEvent(QDropEvent *event){

//}
