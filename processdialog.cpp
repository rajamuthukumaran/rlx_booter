#include "processdialog.h"
#include "ui_processdialog.h"
#include "process.h"

processdialog::processdialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::processdialog)
{
    ui->setupUi(this);
    ui->progress->setMaximum(0);
    ui->progress->setMinimum(0);
}

processdialog::~processdialog()
{
    delete ui;
}

void processdialog::on_cancel_clicked()
{
    Process prcss;
    prcss.ProcessKill();
    this->close();
}
