#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QFileDialog>
#include <QClipboard>
#include <QMessageBox>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_check_btn_clicked();

    void on_brws_btn_clicked();

    void on_start_btn_clicked();

private:
    Ui::MainWindow *ui;

    void getFile();
    bool chksum();
    void liveBooter();
    void deviceLoader();
    bool confirmation(QString,QString);
    void booterOut();
    void disableAll();
    void enableAll();
    void rootchk();
};

#endif // MAINWINDOW_H
